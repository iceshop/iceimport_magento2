<?php
namespace ICEShop\ICEImport\Block;

/**
 * GridBlock block
 */
class GridBlock extends \Magento\Framework\View\Element\Template
{
    protected $_html = '';


    /**
     * @param $data
     * @return string
     */
    public function generateTable($data)
    {
        $return = [];
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '<tr><td>' . $key . ':' . '</td><td>' . $value . '</td></tr>';
        }
        return $html;
    }
}