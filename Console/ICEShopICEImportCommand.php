<?php

namespace ICEShop\ICEImport\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ICEShop\ICEImport\Cron\Load;

class ICEShopICEImportCommand extends Command
{
    private $state;

    protected function configure()
    {
        $this->setName('iceshop:iceimport')->setDescription('Runs the IceImport module.');
    }

    /**
     * ICEShopICEImportCommand constructor.
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(\Magento\Framework\App\State $state) {
        $this->state = $state;
        parent::__construct();
    }

    /**
     * ICEShopICEImportCommand execute command.
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        $load = new Load();
        $load->execute();
    }
}