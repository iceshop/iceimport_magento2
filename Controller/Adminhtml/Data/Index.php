<?php
namespace ICEShop\ICEImport\Controller\Adminhtml\Data;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ProductMetadata;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Magento\Framework\Indexer\StateInterface;
use ICEShop\ICEImport\Cron;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    public $connection;

    public $scopeConfig;

    public $request;

    protected $_objectManager;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;


    }

    protected function getConnection()
    {
        if (!$this->connection) {
            $resource = $this->_objectManager->create('\Magento\Framework\App\ResourceConnection');
            $this->connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->connection;
    }


    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

        if ($this->getRequest()->getParam('start_import') && $this->getRequest()->getParam('isAjax') && $this->getRequest()->getParam('isAjax') == "true" && $this->getRequest()->getParam('start_import') == "yes") {
            $load = new \ICEShop\ICEImport\Cron\Load();
            $load->execute();

            $jsonData = json_encode('success');
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($jsonData);

            $this->scopeConfig = $this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
            $reindex = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/reindex_after_import');

            if ($reindex) {
                $load->reIndex();
            }

        } elseif (($this->getRequest()->getParam('unlock_import') && $this->getRequest()->getParam('isAjax')) && ($this->getRequest()->getParam('isAjax') == "true") && ($this->getRequest()->getParam('unlock_import') == "yes")) {
            $base_path = realpath($directory->getRoot());

            $lock_file_path = $base_path . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'iceshop_iceimport_job.lock';
            if(file_exists($lock_file_path)){
                unlink($lock_file_path);
            }
            $jsonData = json_encode('success');
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($jsonData);

        }
        else {
            $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $this->scopeConfig = $this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

            $this->getConnection();


            $lastStarted = $this->scopeConfig->getValue('iceshop_iceimport_last_started');
            $lastFinished = $this->scopeConfig->getValue('iceshop_iceimport_last_finished');
            $status = $this->scopeConfig->getValue('iceshop_iceimport_status');
            $file_name = $this->scopeConfig->getValue('iceshop_iceimport_imported_file_name');
            $imported_last_time = $this->scopeConfig->getValue('iceshop_iceimport_imported_last_time');
            $count_delete_product = $this->scopeConfig->getValue('iceimport_count_delete_product');
            $errors = $this->scopeConfig->getValue('iceshop_iceimport_errors');


            $block = array();
            $return['Import last started'] = (!empty($lastStarted)) ? date('Y-m-d H:i:s', $lastStarted) : __('Not started');
            $return['Import last finished'] = (!empty($lastFinished)) ? date('Y-m-d H:i:s', $lastFinished) : __('Not finished');;
            $return['Import status'] = (!empty($status)) ? $status : __('Status not available');


            $return['Products imported last time'] = (!empty($imported_last_time)) ? $imported_last_time : 0;
            $return['Removed out of date products last time'] = (!empty($count_delete_product)) ? $count_delete_product : 0;
            $return['Import file name'] = (!empty($file_name)) ? $file_name : __('Import not started yet');
            if($lastFinished < $lastStarted){
                $timeOfImport = '--:--:--';
            } else {
                $timeOfImport = (!empty($lastFinished - $lastStarted)) ? date('H:i:s', $lastFinished - $lastStarted) : __('Can`t calculate');
            }
            $return['Time of import'] = $timeOfImport;
            if(!empty($errors)){
                $return['Errors during last import'] = $errors;
            }


//            $return['Start import manually'] = '<button id="start_manually" title="Start" type="button" class="primary"><span class=""><span>Start</span></span></button>';

            $resultPage = $this->resultPageFactory->create();
            $block['iceimport_information'] = $resultPage->getLayout()
                ->createBlock('ICEShop\ICEImport\Block\GridBlock')
                ->generateTable($return);

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $deployConfig = $objectManager->get('Magento\Framework\App\DeploymentConfig');
            $prefix = (string)$deployConfig->get(
                ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX
            );

            $return = array();
            $return['Start import manually'] = '<button id="start_manually" title="Start" type="button" class="primary"><span class=""><span>Start</span></span></button>';
            $return['Unlock import <span style="font-size:x-small"> (Press ONLY in case when you see message: "Another Iceimport process is running. Please check your `' . $prefix . 'cron_schedule` table !" and you sure that import process stuck)</span>'] =
                '<button id="unlock_import" title="Unlock Import" type="button" class="primary"><span class=""><span>Unlock Import</span></span></button>';

            $return['Download sample file'] = '<button id="download_sample_file" onclick="window.location.href=\'/admin/iceshop_iceimport/data/download\';"  title="Download" type="button" class="primary"><span class=""><span>Download sample file</span></span></button>';

            $block['iceimport_information_actions'] = $resultPage->getLayout()
                ->createBlock('ICEShop\ICEImport\Block\GridBlock')
                ->generateTable($return);

            $jsonData = json_encode($block);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($jsonData);
        }
    }
}

