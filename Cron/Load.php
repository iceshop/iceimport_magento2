<?php

namespace ICEShop\ICEImport\Cron;

use Magento\Cron\Model\Schedule;
use Magento\Framework\App\ResourceConnection;
use ICEShop\ICEImport\Model\Iceimport;
use Magento\Framework\Config\File\ConfigFilePool;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Magento\Cron\Model;

class Load
{
    public function __construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');

        if (!defined('MAGENTO_BP')) {
            define('MAGENTO_BP', $directory->getRoot());
        }
    }

    public function execute()
    {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        if (file_exists(MAGENTO_BP . '/app/etc/env.php')) {
            $configuration = include(MAGENTO_BP . '/app/etc/env.php');
        } else {
            throw new \Exception('Configuration not loaded');
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $deployConfig = $objectManager->get('Magento\Framework\App\DeploymentConfig');
        $tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);
        $resource = $objectManager->create('\Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $lock_file_path = MAGENTO_BP . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'iceshop_iceimport_job.lock';

        if (file_exists($lock_file_path)) {
            $last_start_time = date('Y-m-d H:i:s', file_get_contents($lock_file_path));
            throw new \Exception(__("Another Iceimport process is running. Please check your `{$tablePrefix}cron_schedule` table ! Process was started at: " . $last_start_time));
        } else {
            file_put_contents($lock_file_path, time());
        }

        $connection->query("DELETE FROM {$tablePrefix}iceshop_iceimport_imported_product_ids");

        $scopeConfig = $objectManager->get('\Magento\Framework\App\Config\Storage\WriterInterface');
        $scopeInterface = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $imageProcessOnly = (int)$scopeInterface->getValue('iceshop_iceimport/iceimport_products_parameters/images_queue_processing_only');
        $skipImageProcess = (int)$scopeInterface->getValue('iceshop_iceimport/iceimport_products_settings/skip_process_image_queue');
        $scopeConfig->save('iceshop_iceimport_last_started', time());
        $scopeConfig->save('iceshop_iceimport_status', __('Processing...'));
        $scopeConfig->save('iceimport_count_delete_product', 0);
        $scopeConfig->save('iceshop_iceimport_errors', '');
        try {
            $fast = new Iceimport($configuration);
            if ($imageProcessOnly == 0) {

                $websites = $scopeInterface->getValue('iceshop_iceimport/iceimport_products_mapping/products_websites');

                if ($websites != 'all') {
                    $select_websites_codes = $connection->query("SELECT code FROM {$tablePrefix}store_website WHERE website_id = '$websites';");
                    $websites = $select_websites_codes->fetch()['code'];
                    $fast->importProduct([], $websites);
                } else {
                    $select_websites_codes = $connection->query("SELECT code FROM {$tablePrefix}store_website WHERE code <> 'admin';");
                    $websites_codes = $select_websites_codes->fetchAll(\PDO::FETCH_COLUMN, 0);

                    foreach ($websites_codes as $code) {
                        $fast->importProduct([], $code);
                    }
                }
            }
            if ($skipImageProcess == 0) {
                $fast->processImageQueue();
            }
        } catch (\Exception $e) {
            if (file_exists($lock_file_path)) {
                unlink($lock_file_path);
            }
            $scopeConfig->save('iceshop_iceimport_errors', $e->getMessage());
            throw new \Exception($e->getMessage());
        }

        $scopeConfig->save('iceshop_iceimport_status', __('Finished'));
        $scopeConfig->save('iceshop_iceimport_last_finished', time());
        if (file_exists($lock_file_path)) {
            unlink($lock_file_path);
        }
    }

    public function reIndex()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $indexerFactory = $objectManager->get('\Magento\Indexer\Model\IndexerFactory')->create();
        $collectionFactory = $objectManager->get('\Magento\Indexer\Model\Indexer\CollectionFactory')->create();

        $ids = $collectionFactory->getAllIds();
        foreach ($ids as $id){
            $idx = $indexerFactory->load($id);
            $idx->reindexAll();
        }
    }

}