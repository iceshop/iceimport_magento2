<?php

namespace ICEShop\ICEImport\Model;

use \Magento\Catalog\Model\Product\Visibility as Visibility;
use Magento\Framework\Config\ConfigOptionsListConstants;

class Iceimport
{

    public $arFields = false;

    public $db_magento = null;

    public $db_config = [];

    public $tablePrefix = false;

    public $arrayForPostfixes = [];

    public $stores = [];

    public $count_all_products = 0;

    public $batch = 100000;

    public $end = 0;

    public $tempFolder = 'var/tmp/';

    public $filenameCats = 'file.csv';

    public $filenameValues = 'category_to_product.csv';

    public $tempFeed = 'tempFeed.csv';

    public $tempAttr = 'attr.csv';

    public $optionFlag = true;

    public $configuration = false;

    public $objectManager;

    public $scopeConfig;

    public $writeConfig;

    public $unlinkFlag = false;

    const IMAGE_SUCCESSFUL_DOWNLOADED = 1;

    const IMAGE_ERROR_DOWNLOADED = 2;

    public function __construct($configuration)
    {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->configuration = $configuration;

        $this->scopeConfig = $this->objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

        $this->writeConfig = $this->objectManager->get('\Magento\Framework\App\Config\Storage\WriterInterface');

        $deployConfig = $this->objectManager->get('Magento\Framework\App\DeploymentConfig');

        $dbinfo = [
            "host" => $this->configuration['db']['connection']['default']['host'],
            "user" => $this->configuration['db']['connection']['default']['username'],
            "pass" => $this->configuration['db']['connection']['default']['password'],
            "dbname" => $this->configuration['db']['connection']['default']['dbname']
        ];

        $this->db_config = [
            'host' => $dbinfo["host"],
            'username' => $dbinfo["user"],
            'password' => $dbinfo["pass"],
            'dbname' => $dbinfo["dbname"],
            'driver_options' => [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', \PDO::MYSQL_ATTR_LOCAL_INFILE => true]
        ];

        $this->db_magento = \Zend_Db::factory('Pdo_Mysql', $this->db_config);

        $this->tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);

        $directoryList = $this->objectManager->get(\Magento\Framework\Filesystem\DirectoryList::class);
        $this->tempFolder = $directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . DIRECTORY_SEPARATOR . 'tmp/';
    }

    public function makeFileFromArrayForManualLaunching($importProduct)
    {
        if($this->checkFolderPerms($this->tempFolder)){
            $fp = fopen($this->tempFolder . $this->tempFeed, 'w+');
        }else{
            $fp = fopen($this->tempFeed, 'w+');
        }

        fputcsv($fp, array_keys($importProduct[0]), "\t");

        foreach ($importProduct as $fields) {
            fputcsv($fp, $fields, "\t");
        }

        fclose($fp);
    }

    public function getFileByFTP()
    {
        $file_name = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/import_file_name');
        $url = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/ftp_host');
        $login = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/ftp_user_name');
        $password = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/ftp_password');
        $file_mode = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/file_mode');
        $passive_mode = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/passive_mode');
        $path = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/import_file_path');
        if($this->checkFolderPerms($this->tempFolder)) {
            $tmp_file = $this->tempFolder . time() . '_' . $file_name;
        }else{
            $tmp_file = sys_get_temp_dir() . DIRECTORY_SEPARATOR . time() . '_' . $file_name;
        }
        if (file_exists($tmp_file)) {
            unlink($tmp_file);
        }
        $conn_id = ftp_connect($url);
        if (empty($conn_id)) {
            throw new \Exception('Cannot connect by ftp for shop');
        }

        $login_result = ftp_login($conn_id, $login, $password);
        if (empty($login_result)) {
            ftp_close($conn_id);
            throw new \Exception('Cannot login on ftp server for shop');
        }
        //set passive mode
        if ($passive_mode == 1) {
            ftp_pasv($conn_id, true);
        }

        $size = ftp_size($conn_id, $path . $file_name);
        if ($size) {
            $res = ftp_get($conn_id, $tmp_file, $path . $file_name, $file_mode);
            if (empty($res)) {
                ftp_close($conn_id);
                throw new \Exception('Cannot get file from ftp server for shop');
            }
        } else {
            throw new \Exception('File on FTP not exists or empty');
        }

        ftp_close($conn_id);

        $this->unlinkFlag = true;
        $this->writeConfig->save('iceshop_iceimport_imported_file_name', $file_name);
        return $tmp_file;
    }

    public function getImportFeed()
    {
        $mode = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/data_transfer_type');
        $file_name = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/import_file_name');
        $file_path = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/import_file_path');
        if ($mode === 1) {
            $file = MAGENTO_BP . $file_path . $file_name;
            if (file_exists($file)) {
                $this->writeConfig->save('iceshop_iceimport_imported_file_name', $file_name);
                return $file;
            } else {
                $this->db_magento->query("UPDATE {$this->tablePrefix}cron_schedule SET `status` = 'error', `finished_at` = NOW() WHERE `job_code` = 'iceshop_iceimport_job';");
                $this->writeConfig->save('iceshop_iceimport_last_finished', time());
                throw new \Exception('ICEImport Error: "File path not correct or file not exist"');
            }
        } elseif ($mode === 2) {
            return $this->getFileByFTP();
        }
    }

    public function sendToDb($importProduct = [])
    {
        $filepath = $this->getImportFeed();

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $filepath = str_replace('\\', '/', $filepath);
        }

        if (!empty($importProduct)) {

            $this->makeFileFromArrayForManualLaunching($importProduct);
            $filepath = $this->tempFeed;
        }

        $fieldsForCreatingTable = '`id` INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT, ';

        if($this->checkFolderPerms($this->tempFolder)){
            $fieldsAr = fgetcsv(fopen($filepath, "r"));
        }else{
            $fieldsAr = fgetcsv(fopen($filepath, "r"));
        }

        $fields = str_replace("\t", ',', $fieldsAr[0]);
        $arrayForTable = explode(',', $fields);

        foreach ($arrayForTable as $key => $field) {

            if ($field == "") {
                $this->db_magento->query("INSERT INTO `{$this->tablePrefix}iceshop_extensions_logs` (`log_key`, `log_value`, `log_type`, `timecol`) VALUES ('errorColumn', 'Empty column name', 'stat', NOW());");
                throw new \Exception("Empty column name. \n");
            }

            if (stristr($field, '_id') || stristr($field, 'quantity') || stristr($field, 'is_in_stock') || stristr($field, 'central_stock')) {
                $fieldsForCreatingTable .= "`$field` INT(10) DEFAULT NULL, ";
                $this->arrayForPostfixes['int'][] = $field;
            } elseif ($field == 'sku') {
                $fieldsForCreatingTable .= "`$field` VARCHAR(64) NOT NULL, ";
            } elseif (stristr($field, 'price') || stristr($field, 'weight') || stristr($field, 'cost') || $field == 'qty') {
                $fieldsForCreatingTable .= "`$field` DECIMAL(12,4) DEFAULT NULL, ";
                $this->arrayForPostfixes['decimal'][] = $field;
            } elseif ((stristr($field, 'description') && !stristr($field, 'short_') && !stristr($field, 'long_')) || stristr($field, 'categories') || stristr($field, '_tree')) {
                $fieldsForCreatingTable .= "`$field` TEXT DEFAULT NULL, ";
                $this->arrayForPostfixes['text'][] = $field;
            } elseif (stristr($field, 'tax')) {
                $fieldsForCreatingTable .= "`$field` DECIMAL(12,4) DEFAULT NULL, ";
                $this->arrayForPostfixes['taxes'][] = $field;
            } else {
                $fieldsForCreatingTable .= "`$field` VARCHAR(128) DEFAULT NULL, ";
                $this->arrayForPostfixes['varchar'][] = $field;
            }

            if ($field == 'visibility' || $field == 'status') {
                $this->arrayForPostfixes['int'][] = $field;
            }
        }

        $fieldsForCreatingTable .= '`entity_id` int(10) DEFAULT NULL,`attribute_set_id` smallint(5) DEFAULT NULL, `entity_type_id` smallint(5) DEFAULT NULL, `tax_class_id` int(10) DEFAULT NULL, `is_iceimport` VARCHAR(64) DEFAULT 1, `url_key` VARCHAR(255) DEFAULT NULL,
        `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP';

        $this->arrayForPostfixes['int'][] = 'tax_class_id';
        $this->arrayForPostfixes['varchar'][] = 'is_iceimport';
        $this->arrayForPostfixes['varchar'][] = 'url_key';

        $this->db_magento->query("DROP TABLE IF EXISTS {$this->tablePrefix}import_feed;");
        $this->db_magento->query("CREATE TABLE IF NOT EXISTS {$this->tablePrefix}import_feed($fieldsForCreatingTable) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");

        $this->db_magento->query("LOAD DATA LOCAL INFILE '{$filepath}' IGNORE INTO TABLE {$this->tablePrefix}import_feed CHARACTER SET UTF8 FIELDS TERMINATED BY '\t' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES ($fields);");

        if ($this->unlinkFlag) {
            if (file_exists($filepath)) {
                unlink($filepath);
            }
        }
    }

    public function getDefaultStoreId($website_code)
    {

        $select_website_id = $this->db_magento->query("SELECT website_id FROM `{$this->tablePrefix}store_website` WHERE `code` = '$website_code';");
        $website_id = $select_website_id->fetch()['website_id'];

        $select_default_store_id = $this->db_magento->query("SELECT default_store_id FROM `{$this->tablePrefix}store_group` WHERE website_id = '$website_id';");
        $default_store_id = $select_default_store_id->fetch()['default_store_id'];

        return $default_store_id;
    }

    public function updateFeedTable($importProduct, $websites)
    {

        $this->sendToDb($importProduct);

        $defaulttaxConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/default_tax_for_product');
        $urlKeyPrefixConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/url_key_prefix');

        $select_product_entity_type_id = $this->db_magento->query("SELECT entity_type_id FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_product';");
        $product_entity_type_id = $select_product_entity_type_id->fetch()['entity_type_id'];
        $select_attribute_set_id = $this->db_magento->query("SELECT `default_attribute_set_id` FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_product';");
        $attribute_set_id = $select_attribute_set_id->fetch()['default_attribute_set_id'];
        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `entity_type_id` = $product_entity_type_id;");
        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `attribute_set_id` = $attribute_set_id;");
        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` impf JOIN `{$this->tablePrefix}catalog_product_entity` cpe ON impf.sku = cpe.sku
    SET impf.attribute_set_id = cpe.attribute_set_id;");
        $this->db_magento->query("DELETE FROM `{$this->tablePrefix}import_feed` WHERE sku = '';");

        foreach ($this->arrayForPostfixes['varchar'] as $i) {

            $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET $i = TRIM(BOTH '\"' FROM $i);");
        }

        $visibility_flag = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_visibility_from_csv');

        if (!$visibility_flag) {
            $visibility_value = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/set_visibility');

            $update_visibility = "UPDATE `{$this->tablePrefix}import_feed` SET `visibility` = $visibility_value;";
        } else {
            $update_visibility = "UPDATE `{$this->tablePrefix}import_feed` SET `visibility` = CASE WHEN visibility = 'Not Visible Individually' THEN ". Visibility::VISIBILITY_NOT_VISIBLE. " WHEN visibility = 'Catalog' THEN ". Visibility::VISIBILITY_IN_CATALOG. " WHEN visibility = 'Search' THEN ". Visibility::VISIBILITY_IN_SEARCH. " WHEN visibility = 'Catalog, Search' THEN ". Visibility::VISIBILITY_BOTH. " ELSE visibility END;";
        }

        $this->db_magento->query($update_visibility);

        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `status` = CASE WHEN status = 'Enabled' THEN 1 ELSE 0 END;");
        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `tax_class_id` = $defaulttaxConf;");
        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `is_iceimport` = 1;");
        if ($urlKeyPrefixConf == 1) {
            $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `url_key` = LOWER(TRIM('-' FROM REGEXP_REPLACE(CONCAT(mpn, '-', name), '[^[:alnum:]]+', '-')));");
        } else if ($urlKeyPrefixConf == 2) {
            $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `url_key` = LOWER(TRIM('-' FROM REGEXP_REPLACE(CONCAT(brand_name, '-', mpn, '-', name), '[^[:alnum:]]+', '-')));");
        } else {
            $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `url_key` = LOWER(TRIM('-' FROM REGEXP_REPLACE(name, '[^[:alnum:]]+', '-')));");
        }

        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `created_at` = NOW();");
        $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` SET `updated_at` = NOW();");

        // Update stores
        $default_store_id = $this->getDefaultStoreId($websites);
        $stores_array = $this->db_magento->fetchAll("SELECT `store_id`, `code` FROM {$this->tablePrefix}store WHERE store_id <> 0;");
        $this->stores = $stores_array;
        $query_for_store_update = "UPDATE `{$this->tablePrefix}import_feed` SET `store` = CASE WHEN store = 'default' THEN $default_store_id ";

        foreach ($stores_array as $store) {
            $store_code = $store['code'];
            $store_id = $store['store_id'];
            $query_for_store_update .= "WHEN store = '$store_code' THEN $store_id ";
        }

        $query_for_store_update .= 'ELSE store END;';

        $this->db_magento->query($query_for_store_update);
    }

    public function getUnspscToCatId()
    {

        $select_category_entity_type = $this->db_magento->query("SELECT entity_type_id FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_category';");
        $category_entity_type = $select_category_entity_type->fetch()['entity_type_id'];
        $select_unspsc_id = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = 'unspsc' AND entity_type_id = $category_entity_type;");
        $unspsc_id = $select_unspsc_id->fetch()['attribute_id'];
        $select_unspsc_to_id = $this->db_magento->query("SELECT `entity_id`, `value` FROM  `{$this->tablePrefix}catalog_category_entity_varchar` WHERE attribute_id = $unspsc_id AND store_id = 0;");

        return $select_unspsc_to_id->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public function getUnspscToProdId()
    {

        $select_unspsc_to_id = $this->db_magento->query("SELECT `unspsc`, `entity_id` FROM  `{$this->tablePrefix}stocks_prices_full`;");
        return $select_unspsc_to_id->fetchAll();
    }

    public function addCatIdToArray ($offset)
    {

        $unspscToCatId = $this->getUnspscToCatId();
        $unspscToProdId = $this->db_magento->fetchAll("SELECT `unspsc`, `entity_id` FROM  `{$this->tablePrefix}import_feed` GROUP BY `entity_id`, `unspsc` LIMIT $offset,  $this->batch;");
        $resultArray = [];
        foreach ($unspscToProdId as $key => &$value) {

            foreach ($unspscToCatId as $k => $item) {
                $unspscs = explode(';', $value['unspsc']);

                foreach ($unspscs as $unspsc) {
                    if ($item == $unspsc) {
                        $_tmp = [];
                        $_tmp['unspsc'] = $k;
                        $_tmp['entity_id'] = $value['entity_id'];

                        $resultArray[] = $_tmp;
                    }
                }
            }
        }

        return $resultArray;
    }

    public function makeTempFileForCatalogCategoryProductInsert ($offset)
    {
        $this->deleteTempFileForCatalogCategoryProductInsert();

        $categoriesToProductsConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_categories_to_product');

        $arValues = $this->addCatIdToArray($offset);

        $columns = ['category_id', 'product_id'];

        if($this->checkFolderPerms($this->tempFolder)){
            $fp = fopen($this->tempFolder . $this->filenameValues, 'w+');
        }else{
            $fp = fopen($this->filenameValues, 'w+');
        }

        fputcsv($fp, array_unique($columns), "\t");

        foreach ($arValues as $fields) {
            fputcsv($fp, $fields, "\t");
        }

        fclose($fp);

        // delete categories for set new

        $final = array_map(function ($elements) {
            return $elements['entity_id'];
        }, $arValues);

        if ($categoriesToProductsConf) {
            if (!empty($final)) {
                $this->db_magento->query("DELETE FROM {$this->tablePrefix}catalog_category_product WHERE product_id IN (" . implode(',', $final) . ");");
            }
        }
    }

    public function loadDataToCatalogCategoryProduct()
    {
        $continue = true;
        $counter = 0;

        while ($continue) {

            $offset = $counter * $this->batch;

            $this->makeTempFileForCatalogCategoryProductInsert($offset);

            if($this->checkFolderPerms($this->tempFolder)) {
                $fileName = $this->tempFolder . $this->filenameValues;
                $this->db_magento->query("LOAD DATA LOCAL INFILE '{$fileName}' IGNORE INTO TABLE {$this->tablePrefix}catalog_category_product CHARACTER SET UTF8 FIELDS TERMINATED BY '\t' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES (category_id, product_id);");
            }else{
                $this->db_magento->query("LOAD DATA LOCAL INFILE '{$this->filenameValues}' IGNORE INTO TABLE {$this->tablePrefix}catalog_category_product CHARACTER SET UTF8 FIELDS TERMINATED BY '\t' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES (category_id, product_id);");
            }

            $this->db_magento->query("UPDATE {$this->tablePrefix}catalog_category_product SET position = 1;");
            if ($counter == $this->end) {

                $continue = false;
            }

            $counter++;
        }
    }

    public function getCategoriesEntityId()
    {

        $select_data = $this->db_magento->query("SELECT `entity_id`, `path` FROM `{$this->tablePrefix}catalog_category_entity` WHERE entity_id <> 1 AND entity_id <> 2 ORDER BY entity_id;");
        return $select_data->fetchAll(PDO::FETCH_KEY_PAIR);
    }

    public function insertInCatalogCategoryProduct()
    {

        $this->db_magento->query("INSERT INTO `{$this->tablePrefix}catalog_category_product` (category_id, product_id, position) SELECT spf.category_id, spf.entity_id, 1 FROM {$this->tablePrefix}stocks_prices_full spf ON DUPLICATE KEY UPDATE `position` = 1;;");
    }

    public function prepareArrayForInsertCategories()
    {
        $query = "select unspsc_path, categories path, store
            from {$this->tablePrefix}import_feed";
        $rows = $this->db_magento->fetchAll($query);

        $categories = [];

        foreach ($rows as $row) {

            $path_groups = explode(';', $row['path']);
            $id_groups = explode(';', $row['unspsc_path']);

            foreach ($id_groups as $key => $id_group) {

                $ids = explode('/', $id_group);
                $names = explode('/', $path_groups[$key]);
                $path_string = "";

                foreach ($ids as $id_key => $id) {

                    $unique = $row['store'].'_'.$id;
                    $path_string .= (!empty($path_string) ? "/" : "") . $id;

                    if(!isset($categories[$unique])) {

                        $category = [];
                        $category['category'] = $names[$id_key];
                        $category['unspsc_c'] = $id;
                        $category['unspsc_path'] = $id_group;
                        $category['unspsc_path_c'] = $path_string;
                        $category['path'] = $path_groups[$key];
                        $category['store'] = $row['store'];
                        $category['level'] = $id_key+2;


                        $categories[$unique] = $category;
                    }
                }

            }
        }

        return $categories;

    }


    public function makeTempFileForCategories()
    {
        $this->deleteTempFileForCategories();
        $arCats = $this->prepareArrayForInsertCategories();

        $columns = [];

        foreach ($arCats as &$value) {
            foreach ($value as $key => $item) {
                if ($key == 'path') {
                    unset($value[$key]);
                } else {
                    $columns[] = $key;
                }
            }
        }

        if($this->checkFolderPerms($this->tempFolder)){
            $fp = fopen($this->tempFolder . $this->filenameCats, 'w+');
        }else{
            $fp = fopen($this->filenameValues, 'w+');
        }

        fputcsv($fp, array_unique($columns), "\t");

        foreach ($arCats as $fields) {
            fputcsv($fp, $fields, "\t");
        }

        fclose($fp);

    }

    public function processTemFileForAttributes ($attributes)
    {

        $delimiter = "\t";
        $this->deleteTemFileForAttributes();
        if(file_exists($this->tempAttr)){
            unlink($this->tempAttr);
        }

        if($this->checkFolderPerms($this->tempFolder)){
            $fp = fopen($this->tempFolder . $this->tempAttr, 'w+');
        }else{
            $fp = fopen($this->tempAttr, 'w+');
        }

        foreach ($attributes as $fields) {
            fputcsv($fp, $fields, $delimiter);
            $fields['store'] = 0;
            fputcsv($fp, $fields, $delimiter);
        }

        fclose($fp);
    }

    public function insertIdsInCatalogCategoryEntity()
    {

        $now = date("Y-m-d H:i:s");

        $select_attribute_set_id = $this->db_magento->query("SELECT `default_attribute_set_id` FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_category';");
        $attribute_set_id = $select_attribute_set_id->fetch()['default_attribute_set_id'];

        $select_category_entity_type = $this->db_magento->query("SELECT entity_type_id FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_category';");
        $category_entity_type = $select_category_entity_type->fetch()['entity_type_id'];

        $select_unspsc_id = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = 'unspsc' AND entity_type_id = $category_entity_type;");
        $unspsc_id = $select_unspsc_id->fetch()['attribute_id'];
        $this->db_magento->query("INSERT INTO {$this->tablePrefix}catalog_category_entity (`attribute_set_id`, `path`, `level`, `children_count`, `created_at`, `updated_at`, `position`) SELECT $attribute_set_id, tc.unspsc_path_c, tc.level, '0', '$now', '$now', 0 FROM {$this->tablePrefix}temp_cats tc WHERE unspsc_c NOT IN (SELECT value FROM {$this->tablePrefix}catalog_category_entity_varchar WHERE attribute_id = $unspsc_id AND `value` IS NOT NULL) GROUP BY tc.unspsc_path_c, tc.level;");
    }

    public function updateCategoryPath()
    {

        $unspsc_ids = $this->db_magento->fetchAll("SELECT id, unspsc_c, unspsc_path_c FROM `{$this->tablePrefix}temp_cats`;");

        $select_root_category_id = $this->db_magento->query("SELECT entity_id FROM `{$this->tablePrefix}catalog_category_entity` WHERE level = 0;");
        $root_category_id = $select_root_category_id->fetch()['entity_id'];

        if (!$root_category_id) {
            $this->db_magento->query("INSERT INTO `{$this->tablePrefix}iceshop_extensions_logs` (`log_key`, `log_value`, `log_type`, `timecol`) VALUES ('errorRootCategory', 'There is no root category in shop.', 'stat', NOW());");
            throw new \Exception("There is no root category in your shop. \n");
        }

        $default_category_from_config = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/root_category');

        foreach ($unspsc_ids as $k => &$unspsc_id) {

            $unspsc_id['cat_path'] = $root_category_id . '/' . $default_category_from_config . '/';
            $paths = explode('/', $unspsc_id['unspsc_path_c']);

            foreach ($paths as $path) {
                foreach ($unspsc_ids as $key => $item) {
                    if ($path == $item['unspsc_c']) {
                        $unspsc_id['cat_path'] .= $item['id'] . '/';
                        break;
                    }
                }

                if ($path == $unspsc_ids[$k]['unspsc_c']) {
                    break;
                }
            }

            $unspsc_id['cat_path'] = rtrim($unspsc_id['cat_path'], '/');
            $parent = explode('/', $unspsc_id['cat_path']);
            $unspsc_id['parent_id'] = $parent[count($parent) - 2];
        }

        $query_for_update_path = "UPDATE `{$this->tablePrefix}catalog_category_entity` SET `path` = CASE ";

        foreach ($unspsc_ids as $k => $v) {

            $id = $v['id'];
            $path = $v['cat_path'];
            $query_for_update_path .= "WHEN entity_id = '$id' THEN '$path' ";
        }

        $query_for_update_path .= 'ELSE path END;';

        $this->db_magento->query($query_for_update_path);

        $query_for_update_parent_id = "UPDATE `{$this->tablePrefix}catalog_category_entity` SET `parent_id` = CASE ";

        foreach ($unspsc_ids as $k => $v) {
            $id = $v['id'];
            $parent_id = $v['parent_id'];
            $query_for_update_parent_id .= "WHEN entity_id = '$id' THEN '$parent_id' ";
        }

        $query_for_update_parent_id .= 'ELSE parent_id END;';

        $this->db_magento->query($query_for_update_parent_id);
    }

    public function makeTempTableForCategories()
    {

        $fieldsForCreatingTable = '`row_id` INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT, ';

        if($this->checkFolderPerms($this->tempFolder)){
            $fieldsAr = fgetcsv(fopen($this->tempFolder . $this->filenameCats, "r"));
        }else{
            $fieldsAr = fgetcsv(fopen($this->filenameCats, "r"));
        }

        $fields = str_replace("\t", ',', $fieldsAr[0]);
        $arrayForTable = explode(',', $fields);

        foreach ($arrayForTable as $key => $field) {

            if ($field == 'level' || $field == 'parent_id') {
                $fieldsForCreatingTable .= "`$field` INT(10) DEFAULT NULL, ";
            } else {
                $fieldsForCreatingTable .= "`$field` VARCHAR(128) DEFAULT NULL, ";
            }
        }

        $fieldsForCreatingTable = rtrim($fieldsForCreatingTable, ', ');

        $this->db_magento->query("DROP TABLE IF EXISTS {$this->tablePrefix}temp_cats;");
        $this->db_magento->query("CREATE TABLE IF NOT EXISTS {$this->tablePrefix}temp_cats($fieldsForCreatingTable, id INT(10) DEFAULT NULL, `url_key` VARCHAR(255) DEFAULT NULL) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");


        if($this->checkFolderPerms($this->tempFolder)){
            $filenameCat = $this->tempFolder . $this->filenameCats;
            $this->db_magento->query("LOAD DATA LOCAL INFILE '{$filenameCat}' IGNORE INTO TABLE {$this->tablePrefix}temp_cats CHARACTER SET UTF8 FIELDS TERMINATED BY '\t' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES ($fields);");
        }else{
            $this->db_magento->query("LOAD DATA LOCAL INFILE '{$this->filenameCats}' IGNORE INTO TABLE {$this->tablePrefix}temp_cats CHARACTER SET UTF8 FIELDS TERMINATED BY '\t' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES ($fields);");
        }

    }

    public function getDefaultCountry($store = null)
    {
        return $this->scopeConfig->getValue(
            'general/country/default',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    public function countAllProducts ()
    {

        $select_count_all_products = $this->db_magento->query("SELECT COUNT(DISTINCT sku) as count_all_prods FROM {$this->tablePrefix}import_feed;");
        $this->count_all_products = $select_count_all_products->fetch()['count_all_prods'];
    }

    public function importProduct($importProduct, $websites)
    {

        $this->updateFeedTable($importProduct, $websites);

        $this->countAllProducts();
        $this->end = ceil($this->count_all_products / $this->batch);

        $select_website_id = $this->db_magento->query("SELECT `website_id` FROM {$this->tablePrefix}store_website WHERE `code` = '$websites';");
        $websiteId = $select_website_id->fetch()['website_id'];

        $default_store_id = $this->getDefaultStoreId($websites);

        $countryCode = $this->getDefaultCountry($default_store_id);

        // new products
        $newProducts = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/import_new_products');

        // product attributes
        $producteanConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_gtin');
        $productmpnConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_mpn');
        $productbrandConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_brand');
        $productnameConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_name');
        $productshdescriptionConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_short_description');
        $productdescriptionConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_description');
        $deliveryetaConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_delivery_eta');
        $updateStatusFromCsvConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_status');
        $updateVisibilityFromCsvConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_visibility');
        $updateUrlKeyFromCsvConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_url_key');

        // Images
        $addImages = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/import_products_images_via_cron');

        // p&a
        $productpricesConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_prices');
        $productstockConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_stock');
        $productSalableConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_salable');
        $updateIsInStockFromCsvConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_stock_availability');

        // categories
        $categoriesConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_settings/update_products_category');
        $categoriesToActiveConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/set_to_active_imported_categories');

        // mapping
        $config_mpn = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_mpn');
        $config_brand = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_brand');
        $config_gtin = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_gtin');
        $config_delivery_eta = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_delivery_eta');
        $config_warehouse_code = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_warehouse_code');
        $config_product_state = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_product_state');
        $config_product_icecat_unspsc = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_mapping_product_icecat_unspsc');

        // sorting
        $category_sort = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/sort_categories_in_asc');

        $defaulttaxConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/default_tax_for_product');
        $updateTaxClassConf = (int)$this->scopeConfig->getValue('iceshop_iceimport/icecatconnect_products_mapping/update_tax_class');

        // importing products

        if (!$newProducts) {
            $this->db_magento->query("DELETE FROM `{$this->tablePrefix}import_feed` WHERE sku NOT IN (SELECT sku FROM `{$this->tablePrefix}catalog_product_entity`);");
        }

        //check for empty main table
        $main_table_query = $this->db_magento->query("SELECT COUNT(*) as cnt_rows FROM `{$this->tablePrefix}import_feed` WHERE 1;");
        $main_table_check = $main_table_query->fetch()['cnt_rows'];

        if (!empty($main_table_check)) {

            $this->db_magento->query("UPDATE `{$this->tablePrefix}catalog_product_entity` cpe JOIN {$this->tablePrefix}import_feed impf ON cpe.sku = impf.sku SET cpe.`sku` = impf.sku, cpe.`attribute_set_id` = impf.attribute_set_id, cpe.`type_id` = 'simple', cpe.`updated_at` = impf.updated_at;");

            $this->db_magento->query("
                INSERT INTO `{$this->tablePrefix}catalog_product_entity` 
                (`sku`, `attribute_set_id`, `type_id`, `created_at`, `updated_at`, `updated_ice`) 
                SELECT impf.sku, impf.attribute_set_id, 'simple', impf.created_at, impf.updated_at, DATE(NOW() - INTERVAL 10 YEAR) 
                FROM {$this->tablePrefix}import_feed impf 
                WHERE sku NOT IN (
                    SELECT sku FROM {$this->tablePrefix}catalog_product_entity WHERE sku IS NOT NULL
                ) GROUP BY impf.sku, impf.attribute_set_id, impf.created_at, impf.updated_at;
            ");

            $this->db_magento->query("UPDATE `{$this->tablePrefix}import_feed` impf JOIN `{$this->tablePrefix}catalog_product_entity` cpe ON impf.sku = cpe.sku SET impf.entity_id = cpe.entity_id;");
            $this->db_magento->query("INSERT INTO `{$this->tablePrefix}iceshop_iceimport_imported_product_ids` (`product_id`, `product_sku`) SELECT impf.entity_id, impf.sku FROM `{$this->tablePrefix}import_feed` impf ON DUPLICATE KEY UPDATE product_sku = impf.sku;");
            $this->db_magento->query("INSERT INTO `{$this->tablePrefix}catalog_product_website` (`product_id`, `website_id`) SELECT impf.entity_id, $websiteId FROM {$this->tablePrefix}import_feed impf ON DUPLICATE KEY UPDATE product_id = impf.entity_id, website_id = $websiteId;");

            $select_product_entity_type_id = $this->db_magento->query("SELECT entity_type_id FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_product';");
            $product_entity_type_id = $select_product_entity_type_id->fetch()['entity_type_id'];

            if (!$config_mpn || !$config_brand || !$config_gtin || !$config_delivery_eta) {
                $this->db_magento->query("INSERT INTO `{$this->tablePrefix}iceshop_extensions_logs` (`log_key`, `log_value`, `log_type`, `timecol`) VALUES ('errorMapping', 'Mapings are incorrect.', 'stat', NOW());");
                throw new \Exception("Mapings are incorrect. \n");
            }

            if ($addImages) {
                $this->db_magento->query("INSERT IGNORE INTO `{$this->tablePrefix}iceshop_iceimport_image_queue` (`entity_id`, `image_url` ) SELECT impf.entity_id, impf.image FROM `{$this->tablePrefix}import_feed` impf ;");
            }

            $attrObject = $this->objectManager->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute');
            foreach ($this->arrayForPostfixes as $key => $value) {
                if ($key != 'taxes') {

                    foreach ($value as $k => $attr) {
                        $front_input = false;

                        $attr_for_select = $attr;

                        switch ($attr_for_select) {
                            case 'mpn':
                                $attr_for_select = $config_mpn;
                                break;
                            case 'delivery_eta':
                                $attr_for_select = $config_delivery_eta;
                                break;
                            case 'warehouse_code':
                            case 'warehousecode':
                                if($config_warehouse_code) {
                                    $attr_for_select = $config_warehouse_code;
                                }
                                break;
                            case 'brand_name':
                                $attr_for_select = $config_brand;
                                break;
                            case 'ean':
                                $attr_for_select = $config_gtin;
                                break;
                            case 'product_state':
                                $attr_for_select = $config_product_state;
                                break;
                            case 'icecat_unspsc':
                                $attr_for_select = $config_product_icecat_unspsc;
                                break;
                        }

                        $select_attribute_id = $this->db_magento->query("SELECT `attribute_id`, `frontend_input`, `source_model` FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = '$attr_for_select' AND entity_type_id = $product_entity_type_id;");
                        $tmp_attribute = $select_attribute_id->fetch();
                        $attribute_id = (isset($tmp_attribute['attribute_id'])) ? $tmp_attribute['attribute_id']:null;
                        $frontend_input = (isset($tmp_attribute['frontend_input']))?$tmp_attribute['frontend_input']:null;
                        $source_model = (isset($tmp_attribute['source_model'])) ? $tmp_attribute['source_model']:null;

                        if ($attribute_id) {

                            if ($frontend_input == 'select' && ($source_model == 'Magento\Eav\Model\Entity\Attribute\Source\Table' || empty($source_model))) {
                                $front_input = true;

                                $brands_in_store = [];
                                $elements = [];
                                $brands_in_store = $this->db_magento->fetchAll("SELECT DISTINCT `{$attr}` FROM  {$this->tablePrefix}import_feed WHERE `{$attr}` NOT IN (SELECT `value` FROM {$this->tablePrefix}eav_attribute_option_value eavov LEFT JOIN {$this->tablePrefix}eav_attribute_option eavo ON eavov.option_id = eavo.option_id WHERE eavo.attribute_id = '{$attribute_id}') AND `{$attr}` <> '';");
                                if (!empty($brands_in_store) && $this->optionFlag) {
                                    $final = array_map(function ($elements) use ($attr) {
                                        return $elements[$attr];
                                    }, $brands_in_store);

                                    $attrId = $attrObject->getIdByCode(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE, $attr_for_select);
                                    foreach ($final as $brand_item){
                                        $option = [];
                                        $option['attribute_id'] = $attrId;
                                        $option['value'][][0] = (string)$brand_item;
                                        $this->objectManager->create('Magento\Eav\Setup\EavSetup')->addAttributeOption($option);
                                    }
                                }
                            }

                            $post_fix = $key;
                            if($front_input){
                                $this->db_magento->query("UPDATE {$this->tablePrefix}import_feed impf LEFT JOIN (SELECT eavov.`option_id` as option_id, eavov.`value` FROM {$this->tablePrefix}eav_attribute_option_value eavov LEFT JOIN {$this->tablePrefix}eav_attribute_option eavo ON eavov.option_id = eavo.option_id WHERE eavo.attribute_id = '{$attribute_id}') tmptable ON tmptable.value = impf.{$attr} SET impf.{$attr} = tmptable.option_id WHERE tmptable.option_id IS NOT NULL;");
                            }

                            $continue = true;
                            $counter = 0;

                            while ($continue) {

                                $offset = $counter * $this->batch;
                                if($front_input){
                                    $post_fix = 'int';
                                    $select_attributes = $this->db_magento->query("SELECT $attribute_id, impf.store, impf.entity_id, $attr FROM {$this->tablePrefix}import_feed impf LIMIT $offset, $this->batch;");

                                } else {
                                    $post_fix = $key;
                                    $select_attributes = $this->db_magento->query("SELECT $attribute_id, impf.store, impf.entity_id, $attr FROM {$this->tablePrefix}import_feed impf LIMIT $offset, $this->batch;");
                                }

                                $attributes = $select_attributes->fetchAll();

                                $this->processTemFileForAttributes($attributes);

                                $delimiter = "\t";
                                $filepath = $this->tempAttr;

                                if($this->checkFolderPerms($this->tempFolder)){
                                    $filepath = $this->tempFolder . $filepath;
                                    $this->db_magento->query("LOAD DATA LOCAL INFILE '{$filepath}' IGNORE INTO TABLE {$this->tablePrefix}catalog_product_entity_{$post_fix} CHARACTER SET UTF8 FIELDS TERMINATED BY '$delimiter' ENCLOSED BY '' LINES TERMINATED BY '\n' (attribute_id, store_id, entity_id, value);");
                                }else{
                                    $this->db_magento->query("LOAD DATA LOCAL INFILE '{$filepath}' IGNORE INTO TABLE {$this->tablePrefix}catalog_product_entity_{$post_fix} CHARACTER SET UTF8 FIELDS TERMINATED BY '$delimiter' ENCLOSED BY '' LINES TERMINATED BY '\n' (attribute_id, store_id, entity_id, value);");
                                }

                                if ($counter == $this->end) {

                                    $continue = false;
                                }

                                $counter++;

                            }
                            if ($attr == 'mpn' && $productmpnConf ||
                                $attr == 'ean' && $producteanConf ||
                                $attr == 'brand_name' && $productbrandConf ||
                                $attr == 'delivery_eta' && $deliveryetaConf ||
                                $attr == 'warehouse_code' || $attr == 'warehousecode' ||
                                $attr == 'product_state' || $attr == 'icecat_unspsc' ||
                                $attr == 'description' && $productdescriptionConf ||
                                $attr == 'short_description' && $productshdescriptionConf ||
                                $attr == 'visibility' && $updateVisibilityFromCsvConf ||
                                $attr == 'url_key' && $updateUrlKeyFromCsvConf ||
                                $attr == 'name' && $productnameConf ||
                                $attr == 'price' && $productpricesConf ||
                                $attr == 'special_price' && $productpricesConf ||
                                $attr == 'cost' && $productpricesConf ||
                                $attr == 'tax_class_id' && $defaulttaxConf && $updateTaxClassConf ||
                                $attr == 'status' && $updateStatusFromCsvConf &&
                                $attr != 'image'
                            ) {
                                $this->db_magento->query("UPDATE {$this->tablePrefix}catalog_product_entity_{$post_fix} cpek JOIN {$this->tablePrefix}import_feed impf ON impf.entity_id = cpek.entity_id SET cpek.value = impf.$attr WHERE cpek.attribute_id = $attribute_id AND NOT cpek.value <=> impf.$attr AND cpek.store_id = impf.store;");
                                $this->db_magento->query("UPDATE {$this->tablePrefix}catalog_product_entity_{$post_fix} cpek JOIN {$this->tablePrefix}import_feed impf ON impf.entity_id = cpek.entity_id SET cpek.value = impf.$attr WHERE cpek.attribute_id = $attribute_id AND NOT cpek.value <=> impf.$attr AND cpek.store_id = 0;");

                                if ($attr == 'special_price') {
                                    $this->db_magento->query("UPDATE {$this->tablePrefix}catalog_product_entity_{$post_fix} cpek JOIN {$this->tablePrefix}import_feed impf ON impf.entity_id = cpek.entity_id SET cpek.value = null WHERE cpek.attribute_id = {$attribute_id} AND impf.{$attr} = 0;");
                                }
                            }
                        }
                    }
                }
            }
            $this->deleteTemFileForAttributes();
            if (array_key_exists('taxes', $this->arrayForPostfixes)) {

                foreach ($this->arrayForPostfixes['taxes'] as $tax) {

                    $select_tax_id = $this->db_magento->query("SELECT `attribute_id` FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = '$tax' AND entity_type_id = $product_entity_type_id;");
                    $tax_result = $select_tax_id->fetch();
                    if ($tax_result) {
                        $tax_id = $tax_result['attribute_id'];
                    }

                    if (isset($tax_id)) {
                        $this->db_magento->query("DELETE FROM `{$this->tablePrefix}weee_tax` WHERE attribute_id = '$tax_id';");
                        $this->db_magento->query("INSERT INTO {$this->tablePrefix}weee_tax (website_id, entity_id, country, value, state, attribute_id, entity_type_id) SELECT 0, entity_id, '$countryCode', $tax, '*', $tax_id, $product_entity_type_id FROM {$this->tablePrefix}import_feed;");
                    }
                }
            }

            $stock_name = $this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_mapping/products_stock_name');
            $select_stock_id = $this->db_magento->query("SELECT stock_id FROM `{$this->tablePrefix}cataloginventory_stock` WHERE stock_name = '$stock_name';");
            $stock_id = $select_stock_id->fetch()['stock_id'];

            if ($productstockConf) {
                $this->db_magento->query("UPDATE {$this->tablePrefix}import_feed SET qty = 0 WHERE qty IS NULL;");
                $this->db_magento->query("INSERT INTO {$this->tablePrefix}cataloginventory_stock_item (`product_id`, `stock_id`, `qty`) SELECT impf.entity_id, $stock_id, impf.qty FROM {$this->tablePrefix}import_feed impf 
ON DUPLICATE KEY UPDATE product_id = impf.entity_id, stock_id = $stock_id, qty = impf.qty;");
            } else {
                $this->db_magento->query("INSERT INTO {$this->tablePrefix}cataloginventory_stock_item (`product_id`, `stock_id`, `qty`) SELECT impf.entity_id, $stock_id, impf.qty FROM {$this->tablePrefix}import_feed impf 
ON DUPLICATE KEY UPDATE product_id = impf.entity_id;");
            }

            if ($updateIsInStockFromCsvConf) {
                $this->db_magento->query("UPDATE {$this->tablePrefix}cataloginventory_stock_item AS csi INNER JOIN {$this->tablePrefix}import_feed AS impf ON csi.product_id = impf.entity_id SET csi.is_in_stock = impf.is_in_stock;");
                $this->db_magento->query("INSERT INTO {$this->tablePrefix}cataloginventory_stock_status (`product_id`, `website_id`, `stock_id`, `qty`, `stock_status`) SELECT impf.entity_id, $websiteId, $stock_id, impf.qty, impf.is_in_stock FROM {$this->tablePrefix}import_feed impf
ON DUPLICATE KEY UPDATE product_id = impf.entity_id, stock_id = $stock_id, website_id = $websiteId, qty = impf.qty;");
            } else {
                $this->db_magento->query("INSERT INTO {$this->tablePrefix}cataloginventory_stock_status (`product_id`, `website_id`, `stock_id`, `qty`, `stock_status`) SELECT impf.entity_id, $websiteId, $stock_id, impf.qty, impf.is_in_stock FROM {$this->tablePrefix}import_feed impf
ON DUPLICATE KEY UPDATE product_id = impf.entity_id;");
            }

            $productMetadata = $this->objectManager->get('Magento\Framework\App\ProductMetadataInterface');
            if ($productMetadata->getVersion() >= 2.3 && $productSalableConf) {
                $this->db_magento->query("INSERT INTO {$this->tablePrefix}inventory_source_item (`source_code`, `sku`, `quantity`, `status`) SELECT 'default', impf.sku, impf.qty, impf.is_in_stock FROM {$this->tablePrefix}import_feed impf
ON DUPLICATE KEY UPDATE quantity = impf.qty, status = impf.is_in_stock;");
            }

            // Creating temp file for categories
            $this->makeTempFileForCategories();

            // Creating temp table
            $this->makeTempTableForCategories();

            // catalog_category_entity
            $select_category_entity_type = $this->db_magento->query("SELECT entity_type_id FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_category';");
            $category_entity_type = $select_category_entity_type->fetch()['entity_type_id'];

            $select_unspsc_id = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = 'unspsc' AND entity_type_id = $category_entity_type;");
            $unspsc_id = $select_unspsc_id->fetch()['attribute_id'];

            $select_name_id = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = 'name' AND entity_type_id = $category_entity_type;");
            $name_id = $select_name_id->fetch()['attribute_id'];

            $select_url_key_id = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = 'url_key' AND entity_type_id = $category_entity_type;");
            $url_key_id = $select_url_key_id->fetch()['attribute_id'];

            $this->db_magento->query("UPDATE `{$this->tablePrefix}temp_cats` tc JOIN `{$this->tablePrefix}catalog_category_entity_varchar` ccev ON ccev.value = tc.unspsc_c SET tc.id = ccev.entity_id WHERE attribute_id = $unspsc_id AND tc.id IS NULL;");
            $this->db_magento->query("UPDATE `{$this->tablePrefix}temp_cats` SET `category` = TRIM(BOTH '\"' FROM `category`);");
            $this->db_magento->query("UPDATE `{$this->tablePrefix}temp_cats` SET `url_key` = LOWER(TRIM('-' FROM REGEXP_REPLACE(category, '[^[:alnum:]]+', '-')));");

            $this->insertIdsInCatalogCategoryEntity();
            $this->db_magento->query("UPDATE `{$this->tablePrefix}temp_cats` tc JOIN `{$this->tablePrefix}catalog_category_entity` cce ON cce.path = tc.unspsc_path_c SET tc.id = cce.entity_id WHERE cce.attribute_set_id = {$category_entity_type};");

            $this->updateCategoryPath();

            // catalog_category_entity_varchar
            $array_for_varchar = ['unspsc_c' => $unspsc_id, 'category' => $name_id, 'url_key' => $url_key_id];
            foreach ($array_for_varchar as $attr_varchar => $attr_varchar_id) {

                $store_def_query_varchar = "INSERT INTO {$this->tablePrefix}catalog_category_entity_varchar (`attribute_id`, `store_id`, `entity_id`, `value`) SELECT $attr_varchar_id , tc.store, tc.id, tc.$attr_varchar FROM {$this->tablePrefix}temp_cats tc WHERE tc.id <> 0 ";
                $store_null_query_varchar = "INSERT INTO {$this->tablePrefix}catalog_category_entity_varchar (`attribute_id`, `store_id`, `entity_id`, `value`) SELECT $attr_varchar_id , 0, tc.id, tc.$attr_varchar FROM {$this->tablePrefix}temp_cats tc WHERE store = '$default_store_id' AND tc.id <> 0 ";

                if ($categoriesConf && ($attr_varchar != 'url_key' || $updateUrlKeyFromCsvConf)) {
                    $store_def_query_varchar .= "ON DUPLICATE KEY UPDATE `value` = tc.$attr_varchar;";
                    $store_null_query_varchar .= "ON DUPLICATE KEY UPDATE `value` = tc.$attr_varchar;";

                } else {
                    $store_def_query_varchar .= "ON DUPLICATE KEY UPDATE `entity_id` = tc.id;";
                    $store_null_query_varchar .= "ON DUPLICATE KEY UPDATE `entity_id` = tc.id;";
                }

                $this->db_magento->query($store_def_query_varchar);
                $this->db_magento->query($store_null_query_varchar);
            }

            // catalog_category_entity_int
            if ($categoriesToActiveConf) {
                $array_for_int = ['is_active' => $categoriesToActiveConf, 'is_anchor' => 1, 'include_in_menu' => 1];
            } else {
                $array_for_int = ['is_anchor' => 1, 'include_in_menu' => 1];
            }

            foreach ($array_for_int as $attr_int => $int) {

                $select_attribute_int = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = '$attr_int' AND entity_type_id = $category_entity_type;");
                $attribute_int = $select_attribute_int->fetch()['attribute_id'];

                $store_def_query_int = "INSERT INTO {$this->tablePrefix}catalog_category_entity_int (`attribute_id`, `store_id`, `entity_id`, `value`) SELECT $attribute_int , tc.store, tc.id, $int FROM {$this->tablePrefix}temp_cats tc WHERE tc.id <> 0 ";
                $store_null_query_int = "INSERT INTO {$this->tablePrefix}catalog_category_entity_int (`attribute_id`, `store_id`, `entity_id`, `value`) SELECT $attribute_int , 0, tc.id, $int FROM {$this->tablePrefix}temp_cats tc WHERE store = '$default_store_id' AND tc.id <> 0 ";

                if ($categoriesConf && !$categoriesToActiveConf) {
                    $store_def_query_int .= "ON DUPLICATE KEY UPDATE `value` = $int;";
                    $store_null_query_int .= "ON DUPLICATE KEY UPDATE `value` = $int;";
                } else {
                    $store_def_query_int .= "ON DUPLICATE KEY UPDATE `entity_id` = tc.id;";
                    $store_null_query_int .= "ON DUPLICATE KEY UPDATE `entity_id` = tc.id;";
                }

                $this->db_magento->query($store_def_query_int);
                $this->db_magento->query($store_null_query_int);
            }

            // loading data to catalog_category_product;
            $this->loadDataToCatalogCategoryProduct();
        }

        $this->optionFlag = false;

        //Deleting temp files
        $this->deleteTempFileForCategories();
        $this->deleteTempFileForCatalogCategoryProductInsert();

        // Deleting temp tables
        $this->deleteTempTableCats();
        //$this->deleteTempTableProds();

        if (!empty($importProduct)) {

            $this->deleteFileFromArrayForManualLaunching();
        }


        $count_imported_products_query = $this->db_magento->query("SELECT COUNT(DISTINCT product_sku) as count_imported FROM {$this->tablePrefix}iceshop_iceimport_imported_product_ids WHERE product_sku IS NOT NULL;");
        $count_imported_products = $count_imported_products_query->fetch();
        $this->writeConfig->save('iceshop_iceimport_imported_last_time', (int)$count_imported_products['count_imported']);

        $remove_old_products = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/remove_old_products');
        if ($remove_old_products == 1) {
            $this->deleteOldProducts();
        }

        if ($category_sort) {
            $this->runCategoriesSorting();
        }

        $this->checkAndSetIceField();
        $this->db_magento->query("UPDATE `{$this->tablePrefix}catalog_product_entity_varchar`
SET `value` = TRIM(BOTH '\"' FROM `value`);");
        $this->db_magento->query("UPDATE `{$this->tablePrefix}catalog_product_entity_text`
SET `value` = TRIM(BOTH '\"' FROM `value`);");
    }

    public function runCategoriesSorting()
    {

        $select_number_of_iteration = $this->db_magento->query("SELECT MAX(level) as max FROM `{$this->tablePrefix}catalog_category_entity`;");
        $number_of_iteration = (int)$select_number_of_iteration->fetch()['max'] - 1;
        $update_hide_category = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/hide_empty_categories');
        $position = 1;
        if ($number_of_iteration > 0){
            for ($i = 1; $i <= $number_of_iteration; $i++) {
                $q = "SELECT cce.entity_id FROM `{$this->tablePrefix}catalog_category_entity` cce LEFT JOIN `{$this->tablePrefix}catalog_category_product` ccp ON cce.entity_id = ccp.category_id WHERE ccp.category_id IS NULL AND SUBSTRING_INDEX(cce.path,'/',-1) = cce.entity_id;";
                $empty_categories = $this->db_magento->fetchAll($q);

                $final_array = array_map(function ($element) {
                    return $element['entity_id'];
                }, $empty_categories);

                $catCollection = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Category\Collection')
                    ->addAttributeToSort('name', 'ASC');

                $select_category_entity_type = $this->db_magento->query("SELECT entity_type_id FROM `{$this->tablePrefix}eav_entity_type` WHERE entity_type_code = 'catalog_category';");
                $category_entity_type = $select_category_entity_type->fetch()['entity_type_id'];
                $select_attribute_is_active_id = $this->db_magento->query("SELECT attribute_id FROM `{$this->tablePrefix}eav_attribute` WHERE attribute_code = 'is_active' AND entity_type_id = $category_entity_type;");
                $attribute_is_active_id = $select_attribute_is_active_id->fetch()['attribute_id'];

                $array_for_updating_is_active = [];

                foreach ($catCollection as $key => $category) {
                    $this->db_magento->query("UPDATE `{$this->tablePrefix}catalog_category_entity_int` SET `value` = 1 WHERE `attribute_id` = '{$attribute_is_active_id}' AND `entity_id` = {$category->getId()};");
                    $query = "UPDATE `{$this->tablePrefix}catalog_category_entity` SET position = :position WHERE entity_id = :cat_id ";
                    $this->db_magento->query($query, [
                        ':position' => $position++,
                        ':cat_id' => $category->getId()
                    ]);
                    if ($update_hide_category) {

                        if (in_array($category->getId(), $final_array)) {
                            $count = count($category->getChildrenCategories());

                            if ($count == 0) {
                                $array_for_updating_is_active[] = $category->getId();
                            }
                        }
                    }
                }
                if (!empty($array_for_updating_is_active)) {
                    $ids_for_update_is_active = implode(',', $array_for_updating_is_active);
                    if (!empty($ids_for_update_is_active)) {
                        $this->db_magento->query("UPDATE `{$this->tablePrefix}catalog_category_entity_int` SET `value` = 0 WHERE `attribute_id` = '{$attribute_is_active_id}' AND `entity_id` IN ({$ids_for_update_is_active});");
                    }
                }
            }
        }
    }

    public function deleteOldProducts()
    {
        try {
            $this->db_magento->query("SELECT @is_iceimport_id := `attribute_id`
                                    FROM {$this->tablePrefix}eav_attribute
                                    WHERE attribute_code = 'is_iceimport'");

            $count_prod = $this->db_magento->fetchRow("SELECT count(t2.entity_id) AS count_prod FROM (SELECT cpe.entity_id
                                                        FROM {$this->tablePrefix}catalog_product_entity AS cpe
                                                        JOIN {$this->tablePrefix}catalog_product_entity_varchar AS cpev
                                                            ON cpe.entity_id = cpev.entity_id
                                                            AND cpev.value = 1
                                                            AND cpev.attribute_id = @is_iceimport_id
                                                            GROUP BY cpe.entity_id) t2");

            $count_prod = $count_prod['count_prod'];

            if ($count_prod > 0) {

                $count_del_prod = $this->db_magento->fetchRow("SELECT count(t1.entity_id) AS count__del_prod FROM (SELECT cpe.entity_id
                                                        FROM {$this->tablePrefix}catalog_product_entity AS cpe
                                                        JOIN {$this->tablePrefix}catalog_product_entity_varchar AS cpev
                                                            ON cpe.entity_id = cpev.entity_id
                                                            AND cpev.value = 1
                                                            AND cpev.attribute_id = @is_iceimport_id
                                                        LEFT JOIN {$this->tablePrefix}iceshop_iceimport_imported_product_ids AS iip
                                                            ON cpe.entity_id = iip.product_id
                                                        WHERE iip.product_id IS NULL GROUP BY cpe.entity_id) t1; ");

                if (!empty($count_del_prod['count__del_prod'])) {
                    $count_del_prod = $count_del_prod['count__del_prod'];
                } else {
                    $count_del_prod = 0;
                }

                if ($count_del_prod > 0) {
                    //iceimport products to delete exists, amount > 0
                    $delete_old_products_tolerance = (int)$this->scopeConfig->getValue('iceshop_iceimport/iceimport_products_parameters/tolerance_of_difference');

                    if (round(($count_del_prod / $count_prod * 100), 0) <= $delete_old_products_tolerance) {
                        //iceimport products to delete franction is less than allowed tolerance, deletion approved
                        $this->writeConfig->save('iceimport_count_delete_product', $count_del_prod);
                        $this->db_magento->query("DELETE cpe
                                    FROM {$this->tablePrefix}catalog_product_entity AS cpe
                                    JOIN {$this->tablePrefix}catalog_product_entity_varchar AS cpev
                                        ON cpe.entity_id = cpev.entity_id
                                        AND cpev.value = 1
                                        AND cpev.attribute_id = @is_iceimport_id
                                    LEFT JOIN {$this->tablePrefix}iceshop_iceimport_imported_product_ids AS iip
                                        ON cpe.entity_id = iip.product_id
                                    WHERE iip.product_id IS NULL");

                        $this->db_magento->query("TRUNCATE TABLE {$this->tablePrefix}iceshop_iceimport_imported_product_ids");
                    } else {
                        $error_message = 'Attempt to delete more old products than allowed in Iceimport configuration. Interruption of the process.';
                        $error_message2 = 'Old product percentage: ' . round(($count_del_prod / $count_prod * 100), 2) . '%';
                        $this->db_magento->query("UPDATE {$this->tablePrefix}cron_schedule SET `status` = 'error', `finished_at` = NOW() WHERE `job_code` = 'iceshop_iceimport_job';");
                        $this->writeConfig->save('iceshop_iceimport_last_finished', time());

                        //flag to warning notice
                        throw new \Exception("$error_message $error_message2. \n");

                    }
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function processImageQueue()
    {

        $queueList = $this->db_magento->fetchAll("SELECT `queue_id`, `entity_id`, `image_url` FROM `{$this->tablePrefix}iceshop_iceimport_image_queue` WHERE `is_downloaded` = 0");
        if (count($queueList) > 0) {
            $dir = $this->objectManager->get('Magento\Framework\Filesystem');
            $directory_list = $this->objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
            $mediaDir = $directory_list->getPath('media');
            foreach ($queueList as $queue) {
                $queueId = $queue['queue_id'];
                $productId = $queue['entity_id'];
                $imageUrl = $queue['image_url'];

                $preImageName = explode('/', $imageUrl);
                $imageName = array_pop($preImageName);
                if (file_exists($mediaDir . DIRECTORY_SEPARATOR . $imageName)) {
                    $imageName = rand() . '_' . time() . $imageName;
                }
                try{
                    $image_content = file_get_contents($imageUrl);
                } catch (\Exception $e){
                    $this->setImageAsDownloadedError($queueId);
                    continue;
                }

                if (file_put_contents($mediaDir . DIRECTORY_SEPARATOR . $imageName, file_get_contents($imageUrl))) {

                    $types = ['small_image', 'thumbnail', 'image'];
                    $product = $this->objectManager->create('\Magento\Catalog\Model\Product')->load(
                        $productId
                    );
                    $product->addImageToMediaGallery($mediaDir . DIRECTORY_SEPARATOR . $imageName, $types, false, false, true);
                    $product->setUrlKey($product->formatUrlKey($product->getName() . '-' . $product->getStore()->getId().time()));
                    $product->save();
                    $this->setImageAsDownloaded($queueId);
                    unset($product);
                } else {
                    $this->setImageAsDownloadedError($queueId);
                    continue;
                }
            }
        }
    }

    public function setImageAsDownloaded($queueId = false)
    {

        if ($queueId) {
            $this->db_magento->query(
                "UPDATE `{$this->tablePrefix}iceshop_iceimport_image_queue`
                    SET is_downloaded = " . self::IMAGE_SUCCESSFUL_DOWNLOADED . "
                    WHERE queue_id = :queue_id",
                [':queue_id' => $queueId]
            );
        }
    }

    public function setImageAsDownloadedError($queueId = false)
    {

        if ($queueId) {
            $this->db_magento->query(
                "UPDATE `{$this->tablePrefix}iceshop_iceimport_image_queue`
                    SET is_downloaded = " . self::IMAGE_ERROR_DOWNLOADED . "
                    WHERE queue_id = :queue_id",
                [':queue_id' => $queueId]
            );
        }
    }

    public function checkAndSetIceField()
    {
        $optionId = false;
        $eav = $this->objectManager->get('\Magento\Eav\Model\Config');
        $attribute = $eav->getAttribute('catalog_product', 'active_ice')->getData();
        if (isset($attribute['attribute_id'])) {

            $sql = "DROP PROCEDURE IF EXISTS FIELD_EXISTS;";
            $this->db_magento->query($sql);

            $sql = "CREATE PROCEDURE FIELD_EXISTS(
                    OUT _exists    BOOLEAN, -- return value
                    IN  tableName  CHAR(255) CHARACTER SET 'utf8', -- name of table to look for
                    IN  columnName CHAR(255) CHARACTER SET 'utf8', -- name of column to look for
                    IN  dbName     CHAR(255) CHARACTER SET 'utf8'       -- optional specific db
                ) BEGIN
                -- try to lookup db if none provided
                    SET @_dbName := IF(dbName IS NULL, database(), dbName);

                    IF CHAR_LENGTH(@_dbName) = 0
                    THEN -- no specific or current db to check against
                        SELECT
                            FALSE
                        INTO _exists;
                    ELSE -- we have a db to work with
                        SELECT
                            IF(count(*) > 0, TRUE, FALSE)
                        INTO _exists
                        FROM information_schema.COLUMNS c
                        WHERE
                            c.TABLE_SCHEMA = @_dbName
                            AND c.TABLE_NAME = tableName
                            AND c.COLUMN_NAME = columnName;
                    END IF;
                END;";
            $this->db_magento->query($sql);

            $sql = "CALL FIELD_EXISTS(@_exists, '{$this->tablePrefix}catalog_product_entity', 'active_ice', NULL);";
            $this->db_magento->query($sql);

            $sql = "SELECT @_exists;";
            $res = $this->db_magento->fetchCol($sql);
            if (array_shift($res)) {
                $options = $eav->getAttribute('catalog_product', 'active_ice')->getSource()->getAllOptions();
                foreach ($options as $option) {
                    if ($option['label'] == 'Yes') {
                        $optionId = $option['value'];
                        break;
                    }
                }
                if ($optionId) {
                    $sql = "UPDATE `{$this->tablePrefix}catalog_product_entity` SET `active_ice` = '{$optionId}' WHERE `active_ice` IS NULL;";
                    $this->db_magento->query($sql);
                }
            }
        }
    }


    public function deleteTempTableProds()
    {

        $this->db_magento->query("DROP TABLE IF EXISTS {$this->tablePrefix}import_feed;");
    }

    public function deleteTempTableCats()
    {

        $this->db_magento->query("DROP TABLE IF EXISTS {$this->tablePrefix}temp_cats;");
    }

    public function deleteTempFileForCategories()
    {
        if (file_exists($this->filenameCats)) {
            unlink($this->filenameCats);
        }

    }

    public function deleteTempFileForCatalogCategoryProductInsert()
    {
        if (file_exists($this->filenameValues)) {
            unlink($this->filenameValues);
        }
    }

    public function deleteFileFromArrayForManualLaunching()
    {
        if (file_exists($this->tempFeed)) {
            unlink($this->tempFeed);
        }
    }

    public function deleteTemFileForAttributes ()
    {

        if (file_exists($this->tempAttr)) {
            unlink($this->tempAttr);
        }
    }

    public function checkFolderPerms($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
        if (is_readable($dir)) {
            if (is_writable($dir)) {
                return true;
            }
        }
        return false;
    }
}