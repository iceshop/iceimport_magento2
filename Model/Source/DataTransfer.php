<?php
namespace ICEShop\ICEImport\Model\Source;

class DataTransfer implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '0' => __('Interactive'),
            '1' => __('Local/Remote Server'),
        ];
    }

}