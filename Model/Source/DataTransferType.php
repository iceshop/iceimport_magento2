<?php
namespace ICEShop\ICEImport\Model\Source;

class DataTransferType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '1' => __('Local Server'),
            '2' => __('Remote FTP'),
        ];
    }

}