<?php
namespace ICEShop\ICEImport\Model\Source;

use Magento\Framework\Config\ConfigOptionsListConstants;

class Defaulttax implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $optonsList = [
            '' => "--- " . __('Choose tax class') . " ---",
        ];

        $magentoTaxClassesList = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('Magento\Tax\Model\TaxClass\Source\Product')
            ->getAllOptions(false);

        foreach($magentoTaxClassesList as $option) {
            $optonsList[$option['value']] = $option['label'];
        }

        return $optonsList;
    }
}