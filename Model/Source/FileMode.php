<?php
namespace ICEShop\ICEImport\Model\Source;

class FileMode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            FTP_BINARY => __('BINARY'),
            FTP_ASCII => __('ASCII'),
        ];
    }

}