<?php
namespace ICEShop\ICEImport\Model\Source;

class Information implements \Magento\Framework\Option\ArrayInterface
{

    protected $_urlBuilder;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlBuilder
    )
    {
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => $this->_urlBuilder->getUrl("iceshop_iceimport/data/index"),
                'label' => __('')
            ],
        ];
    }

}