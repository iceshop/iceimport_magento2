<?php

namespace ICEShop\ICEImport\Model\Source;

use Magento\Framework\Config\ConfigOptionsListConstants;

class Stock implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        $return = [
            '' => "--- " . __('Choose stock') . " ---"
        ];

        $om = \Magento\Framework\App\ObjectManager::getInstance();

        $deployConfig = $om->get('Magento\Framework\App\DeploymentConfig');
        $prefix = (string)$deployConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX
        );
        $resource = $om->create('\Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

        $sql = "SELECT * FROM {$prefix}cataloginventory_stock WHERE 1";
        $query = $connection->fetchAll($sql);

        if (!empty($query)) {
            foreach ($query as $key => $value) {
                $return[$value['stock_name']] = $value['stock_name'];
            }
        }
        asort($return);

        return $return;
    }

}