<?php
namespace ICEShop\ICEImport\Model\Source;

class UrlKeyPrefixOptions implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return array(
            0 => 'None',
            1 => 'MPN',
            2 => 'Brand + MPN'
        );
    }
}