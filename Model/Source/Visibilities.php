<?php

namespace ICEShop\ICEImport\Model\Source;

use \Magento\Store\Model\StoreManager;
use \Magento\Catalog\Model\Product\Visibility as Visibility;

class Visibilities implements \Magento\Framework\Option\ArrayInterface
{

    protected $_urlBuilder;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlBuilder
    )
    {
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        $paramsArray = array('' => '--Choose the attribute--');
        $paramsArray[Visibility::VISIBILITY_NOT_VISIBLE] = 'Not visible';
        $paramsArray[Visibility::VISIBILITY_IN_CATALOG] = 'Visible in catalog';
        $paramsArray[Visibility::VISIBILITY_IN_SEARCH] = 'Visible in search';
        $paramsArray[Visibility::VISIBILITY_BOTH] = 'Visible in catalog and in search';

        return $paramsArray;
    }

}