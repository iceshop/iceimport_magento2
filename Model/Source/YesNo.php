<?php
namespace ICEShop\ICEImport\Model\Source;

class YesNo implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '1' => __('Yes'),
            '0' => __('No'),
        ];
    }

}