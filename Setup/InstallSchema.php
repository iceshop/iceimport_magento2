<?php
namespace ICEShop\ICEImport\Setup;


use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class InstallSchema implements InstallSchemaInterface
{
    /*
     * Connection variable
     */
    private $_connection = null;

    private $_objectManager = null;


    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_createObjectManager();

        $this->_getConnection();

        $eavFactory = $this->_objectManager->get('Magento\Quote\Setup\QuoteSetup');

        /** @var $installerCategories \Magento\Catalog\Setup\CategorySetup */
        $installerCategories = $this->_objectManager->create('Magento\Catalog\Setup\CategorySetup');
        $entityTypeId = $installerCategories->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
        $attributeSetId = $installerCategories->getDefaultAttributeSetId($entityTypeId);
        $attributeGroupId = $installerCategories->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

        /**
         * Creating table to contain conversions types
         */

        $eavFactory->addAttribute('catalog_category', 'unspsc', [
            'type' => 'varchar',
            'label' => 'unspsc',
            'input' => 'text',
            'global' => \Magento\Catalog\Model\Category\Attribute::SCOPE_STORE,
            'visible' => true,
            'required' => false,
            'user_defined' => false,
            'default' => 0
        ]);

        $installerCategories->addAttributeToGroup(
            $entityTypeId,
            $attributeSetId,
            $attributeGroupId,
            'unspsc',
            '11'
        );
        $attributeId = $installerCategories->getAttributeId($entityTypeId, 'unspsc');

        $sql = "DROP TABLE IF EXISTS `{$setup->getTable('iceimport_imported_product_ids')}`;";
        $this->_connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `{$setup->getTable('capacity_product_image_queue')}`;";
        $this->_connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `{$setup->getTable('iceshop_iceimport_image_queue')}`;";
        $this->_connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `{$setup->getTable('iceshop_iceimport_imported_product_ids')}`;";
        $this->_connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `{$setup->getTable('iceshop_extensions_logs')}`;";
        $this->_connection->query($sql);

        $sql = "INSERT IGNORE INTO `{$setup->getTable('catalog_category_entity_varchar')}`
        (`attribute_id`, `entity_id`, `value`)
        SELECT '{$attributeId}', `entity_id`, '1'
        FROM `{$setup->getTable('catalog_category_entity')}`;";
        $this->_connection->query($sql);


        $sql = "CREATE TABLE IF NOT EXISTS `{$setup->getTable('iceshop_iceimport_image_queue')}` (
          `queue_id`  INT(10) NOT NULL AUTO_INCREMENT,
          `entity_id` INT(10) UNSIGNED NOT NULL,
          `image_url` VARCHAR(255) NOT NULL,
          `is_downloaded` TINYINT NOT NULL DEFAULT 0,
          PRIMARY KEY(`queue_id`),
          UNIQUE KEY (`entity_id`, `image_url`),
          CONSTRAINT `FK_CAP_PRD_IMG_QUEUE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `{$setup->getTable('catalog_product_entity')}` (`entity_id`) ON DELETE CASCADE
        )ENGINE=InnoDB CHARSET=utf8 COMMENT='Table to manage product image import';";
        $this->_connection->query($sql);


        $sql = "CREATE TABLE IF NOT EXISTS `{$setup->getTable('iceshop_iceimport_imported_product_ids')}` (
          `product_id` int(11) NOT NULL,
          `product_sku` varchar(255) DEFAULT NULL,
          KEY `pi_idx` (`product_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->_connection->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS {$setup->getTable('iceshop_extensions_logs')} (
            `log_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `log_key` VARCHAR(255) NOT NULL,
            `log_value` TEXT,
            `log_type` VARCHAR(10) NOT NULL DEFAULT 'info',
            `timecol` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            UNIQUE KEY (`log_key`),
            KEY `IDX_LOG_TYPE` (`log_type`),
            KEY `IDX_TIMECOL` (`timecol`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Iceshop Connector logs';";
        $this->_connection->query($sql);

        $sql = "CREATE PROCEDURE FIELD_EXISTS_ICEIMPORT(
                    OUT _exists    BOOLEAN, -- return value
                    IN  tableName  CHAR(255) CHARACTER SET 'utf8', -- name of table to look for
                    IN  columnName CHAR(255) CHARACTER SET 'utf8', -- name of column to look for
                    IN  dbName     CHAR(255) CHARACTER SET 'utf8'       -- optional specific db
                ) BEGIN
                -- try to lookup db if none provided
                    SET @_dbName := IF(dbName IS NULL, database(), dbName);
                
                    IF CHAR_LENGTH(@_dbName) = 0
                    THEN -- no specific or current db to check against
                        SELECT
                            FALSE
                        INTO _exists;
                    ELSE -- we have a db to work with
                        SELECT
                            IF(count(*) > 0, TRUE, FALSE)
                        INTO _exists
                        FROM information_schema.COLUMNS c
                        WHERE
                            c.TABLE_SCHEMA = @_dbName
                            AND c.TABLE_NAME = tableName
                            AND c.COLUMN_NAME = columnName;
                    END IF;
                END;";
        $this->_connection->multiQuery($sql);

        $sql = "CALL FIELD_EXISTS_ICEIMPORT(@_exists, '{$setup->getTable('catalog_product_entity')}', 'updated_ice', NULL);";
        $this->_connection->query($sql);
		$sql = "SELECT @_exists;";
		$res = $this->_connection->fetchCol($sql);
		if (!array_shift($res)) {
			$sql = "ALTER TABLE `{$setup->getTable('catalog_product_entity')}` 
				ADD COLUMN `updated_ice` TIMESTAMP DEFAULT 0 COMMENT 'Iceshop Update Time';";
			$this->_connection->query($sql);
		}

        $sql = "INSERT IGNORE INTO `{$setup->getTable('iceshop_extensions_logs')}`
            SET `log_key` = 'first_start', `log_value` = 'yes', `log_type` = 'info';
    ";
        $this->_connection->query($sql);

        $setup->endSetup();
    }

    protected function _getConnection()
    {
        $this->_createObjectManager();
        if (!$this->_connection) {
            $resource = $this->_objectManager->create('\Magento\Framework\App\ResourceConnection');
            $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->_connection;
    }

    private function _createObjectManager()
    {
        if (empty($this->_objectManager)) {
            $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        }
    }
}