<?php

namespace ICEShop\ICEImport\Test\Unit\Model;

use ICEShop\ICEImport\Model\Iceimport;
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Http;
use Magento\Framework\App\Area;
use Magento\Framework\App\State as AppState;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\Filesystem;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Vendor\Module\Block\Block as TestingBlock;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager as ObjectManager;
use ICEShop\ICEImport\Test\Unit\ObjectManagerFactory as TestObjectManagerFactory;

class ActionTest extends \PHPUnit_Framework_TestCase
{

    protected $objectManager;

    protected $realObjectManager;

    protected $appState;

    protected $storeManager;

    protected $resource = null;

    protected $iceimport = null;

    protected function initRealObjectManager ()
    {
        $realObjectManagerFactory = new TestObjectManagerFactory();
        $this->realObjectManager = $realObjectManagerFactory->create();
        $frontendConfigurations = $this->realObjectManager
            ->get(ConfigLoader::class)
            ->load(Area::AREA_FRONTEND);
        $this->realObjectManager->configure($frontendConfigurations);
    }

    public function setUp ()
    {

        if (!defined('MAGENTO_BP')) {
            define('MAGENTO_BP', realpath(dirname(__DIR__) . '/../../../../../'));
        }

        if (file_exists(MAGENTO_BP . '/etc/env.php')) {
            $configuration = include(MAGENTO_BP . '/etc/env.php');
        } else {

            throw new \Exception('Configuration not loaded');
        }

        $this->objectManager = new ObjectManager($this);
        $this->initRealObjectManager();

        $this->appState = $this->realObjectManager->get(AppState::class);
        $this->appState->setAreaCode(Area::AREA_FRONTEND);

        $this->storeManager = $this->realObjectManager->create('\Magento\Store\Model\StoreManagerInterface');

        $this->iceimport = new Iceimport($configuration);
    }

    public function testGetDefaultStoreId ()
    {
        $defaultStoreId = $this->storeManager->getStore()->getStoreId();

        $this->assertEquals($defaultStoreId, $this->iceimport->getDefaultStoreId());
    }
}
