<?php

/**
 * @author ICEShop Team
 * @copyright Copyright (c) 2016 ICEShop (https://www.iceshop.biz/)
 * @package ICEShop_ICEImport
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'ICEShop_ICEImport',
    __DIR__
);