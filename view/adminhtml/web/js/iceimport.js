
require(['jquery', 'mage/template', 'jquery/ui', 'mage/translate', 'mage/loader', 'loaderAjax'], function ($) {

    var preloader_data = jQuery('#html-body').attr('data-mage-init');

    var flag = false;

    if (typeof (preloader_data) != 'undefined') {
        flag = true;
    }

    if (flag) {
        var obj = jQuery.parseJSON(preloader_data);
        var preloader_src = obj.loader.icon;
        var template = '<div id = "custom_loader" class="loading-mask" data-role="loader_custom" style="display: block;"><div class="popup popup-loading"><div class="popup-inner"><img alt="Loading..." src="{src_url}">Please wait...</div></div></div>';
        var preloader_template = template.replace('{src_url}', preloader_src);
    }
    var refresh_url = false;
    var button = '<button id="reload_button" title="Reload" type="button" class="primary"><span class=""><span>Reload</span></span></button>';
    //check elements on page
    var check_empty_field = jQuery(".empty_field").length;

    if (check_empty_field > 0) {
        //clear groups
        jQuery(".empty_field").each(function () {
            jQuery(this).parents('tbody').html('');
        });
    }


    jQuery(document).ready(function () {
        getTabsContent();
    });


    function getTabsContent() {
        //check url for send ajax request
        var check_existing_url = jQuery("#iceshop_iceimport_information_iceimport_information_path_url").length;

        if (check_existing_url > 0) {
            if (flag) {
                jQuery('#html-body').append(preloader_template);
            }
            jQuery(document).ready(function () {
                var url = jQuery("#iceshop_iceimport_information_iceimport_information_path_url").find('option')[0].value;
                refresh_url = url;
                //ajax request
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    context: jQuery('#html-body'),
                }).done(function (response) {
                    for (var key in response) {
                        jQuery("#iceshop_iceimport_information_" + key).find('tbody').html(response[key]);
                    }
                    jQuery('#save').remove();
                    jQuery('.page-actions').append(button);
                    jQuery('#custom_loader').remove();
                }).fail(function (response) {
                    alert('There is some error with Ajax request');
                    jQuery('#custom_loader').remove();
                })

                ;
            });
        }
    }

    jQuery(document).on('click', '#reload_button', function () {
        // jQuery('#html-body').append(preloader_template);
        if (refresh_url != false) {
            $.ajax({
                url: refresh_url,
                type: 'get',
                dataType: 'json',
                context: jQuery('#html-body'),
            }).done(function (response) {
                for (var key in response) {
                    jQuery("#iceshop_iceimport_information_" + key).find('tbody').html(response[key]);

                }
                jQuery('#custom_loader').remove();
            }).fail(function (response) {
                alert('There is some error with Ajax request');
                jQuery('#custom_loader').remove();
            });
        }
        jQuery('#custom_loader').remove();
    });

    jQuery(document).on('click', '#start_manually', function () {
        if (confirm("Are you sure that want start import ? (Please note: Imports for big assortment need start via Magento Cron functionality )") == true) {
            jQuery('#start_manually').prop('disabled', true);
            manualStart();
        }
    });

    jQuery(document).on('click', '#unlock_import', function () {
        if (confirm("Please, agree wit it only if you sure that import process not running ! In another case import can work not correctly !") == true) {
            unlockImport();
        }
    });

    function manualStart(){
        if (flag) {
            jQuery('#html-body').append(preloader_template);
        }
        if (refresh_url != false) {
            $.ajax({
                url: refresh_url,
                data: {
                    'start_import': 'yes'
                },
                type: 'get',
                dataType: 'json',
                context: jQuery('#html-body'),
            }).success(function (response) {
                if (response == 'success') {
                    jQuery('#reload_button').click();
                    alert('Products were imported!');
                } else {
                    alert('There is some error with Ajax request');
                    jQuery('#custom_loader').remove();
                }
                jQuery('#start_manually').prop('disabled', false);

            }).fail(function (response) {
                var div = document.createElement("div");
                div.innerHTML = response.responseText;
                var text = div.textContent || div.innerText || "";
                alert('There is some error with Ajax request "' + text + '"; ');
                jQuery('#custom_loader').remove();
            })

            ;
        }
    }

    function unlockImport(){
        if (flag) {
            jQuery('#html-body').append(preloader_template);
        }        if (refresh_url != false) {
            $.ajax({
                url: refresh_url,
                data: {
                    'unlock_import': 'yes'
                },
                type: 'get',
                dataType: 'json',
                context: jQuery('#html-body'),
            }).success(function (response) {
                if (response == 'success') {
                    jQuery('#reload_button').click();
                    alert('Import unlocked !');
                } else {
                    alert('There is some error with Ajax request');
                    jQuery('#custom_loader').remove();
                }
            }).fail(function (response) {
                var div = document.createElement("div");
                div.innerHTML = response.responseText;
                var text = div.textContent || div.innerText || "";
                alert('There is some error with Ajax request "' + text + '"; ');
                jQuery('#custom_loader').remove();
            })

            ;
        }
    }


});