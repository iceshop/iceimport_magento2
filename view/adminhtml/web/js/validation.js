require([
        'jquery',
        'mage/translate',
        'jquery/validate'],
    function($){
        $.validator.addMethod(
            'validate-filename', function (v) {
                return (v.toLowerCase().lastIndexOf(".csv") != -1);
            }, $.mage.__('Please upload a file with .csv extension'));
    }
);